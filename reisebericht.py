#!/usr/bin/python2.7
# -*- coding: utf-8 -*- 

# Quoting styles python 
# https://google.github.io/styleguide/pyguide.html
# module_name, package_name, ClassName, method_name, 
# ExceptionName, function_name, GLOBAL_CONSTANT_NAME, 
# global_var_name, instance_var_name, 
# function_parameter_name, local_var_name

import sys
import datetime
import re
import mysql.connector
import string
import os
import urllib
#from mysql import connector
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus.flowables import HRFlowable
from reportlab.lib.units import cm
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.lib import utils

os.chdir(str(os.path.dirname(os.path.abspath(__file__))))

cnx=None

try:
    cnx = mysql.connector.connect(user=sys.argv[1].strip(), password=sys.argv[2],
                              host=sys.argv[3],
                              database=sys.argv[4])
except mysql.connector.errors.InterfaceError:
    print("Cannot connect to Database")
    exit()

cursor = cnx.cursor()

topic_id=int(sys.argv[5])
username=sys.argv[6]
poster_id=sys.argv[7]
picturePath=sys.argv[8]


def makeSafeFilename(inputFilename):
    safechars = string.letters + string.digits + "~ -_."

    try:
        return filter(lambda c: c in safechars, inputFilename)
    except:
        return "" 

def get_UTC_as_Date(utc):
    return datetime.datetime.fromtimestamp(utc)

def get_topic_title():
    query = ("SELECT post_subject FROM hhlforum3_posts WHERE topic_id=%s AND poster_id=%s ORDER BY post_time ASC LIMIT 1")
    cursor.execute(query,(topic_id, poster_id)) 
    return cursor.fetchone()[0]

def get_posts():
    query = ("SELECT post_text, post_attachment, post_time, post_id FROM hhlforum3_posts WHERE topic_id=%s AND poster_id=%s ORDER BY post_time ASC")
    cursor.execute(query,(topic_id, poster_id)) 
    postList=[]
    for i in cursor:
        postList.append(i)
    return postList

def get_pictures_of_post(post_id):
    query = ("SELECT physical_filename FROM hhlforum3_attachments WHERE topic_id=%s AND post_msg_id=%s")
    cursor.execute(query,(topic_id,post_id))
    return cursor.fetchall()

def get_picture(real_filename):
    query = ("SELECT physical_filename,extension FROM hhlforum3_attachments WHERE topic_id=%s AND real_filename=%s")
    cursor.execute(query,(topic_id,real_filename))
    result=cursor.fetchall()
    picture=picturePath+result[0][0].decode('utf-8')

    return picture

def getImage(path, width):
    img = utils.ImageReader(path)
    iw, ih = img.getSize()
    aspect = ih / float(iw)
    return Image(path, width=width, height=width*aspect)

def getImageH(path, height):
    img = utils.ImageReader(path)
    iw, ih = img.getSize()
    return Image(path, width=height*iw/float(ih), height=height)

def buildDoc():
    topic_title=get_topic_title()
    filename=makeSafeFilename(topic_title+"-"+str(topic_id)+"-"+username+".pdf")
    if len(sys.argv) > 9:
        filename=makeSafeFilename(sys.argv[9])

    doc = SimpleDocTemplate(filename,
                        rightMargin=72,leftMargin=72,
                        topMargin=10,bottomMargin=18)

    report=[]

    styles=getSampleStyleSheet()
    title= '<para alignment="center"><font size=16><strong>%s</strong></font></para>' %(topic_title)
    ptext = '<para alignment="center"><font size=12>%s</font></para>' %("Bericht von " + username)
    report.append(Paragraph(title, styles["Normal"]))
    report.append(Spacer(1, 12))
    report.append(Paragraph(ptext, styles["Normal"]))
    report.append(Spacer(1, 12))

    posts=get_posts()
    img_to_delete=[]
    for post in posts:
        date='<para alignment="right"><font size=6>%s</font></para>' %("Post vom "+get_UTC_as_Date(post[2]).strftime("%d.%m.%Y um %H:%M"))
        report.append(Paragraph(date, styles["Normal"]))
        report.append(Spacer(1, 5))

        image_list=[]

        post_text=post[0].replace('\n','<br />\n')
        post_text = re.sub(r"class=\".*?\"", "color=blue",post_text)
        post_text = re.sub(r"onclick=\".*?\"", "",post_text)
        post_text = re.sub(r"<img.*?>", "",post_text)

        if not re.search("\[attachment.*?-->(.*?)<!--.*?-->\[/attachment:.*?]",post_text) and not re.search("\[img:.*\](.*?)\[/img:.*?\]",post_text):
            text= '<para alignment="left"><font size=8>%s</font></para>' %(post_text)
            report.append(Paragraph(text, styles["Normal"]))
            report.append(Spacer(1, 4))
        else:
            while re.search("\[attachment.*?-->(.*?)<!--.*?-->\[/attachment:.*?]",post_text) or re.search("\[img:.*\](.*?)\[/img:.*?\]",post_text):
                url_img=len(post_text)
                attach_img=len(post_text)

                if re.search("\[img:.*\](.*?)\[/img:.*?\]",post_text):
                    url_img=re.search("\[img:.*\](.*?)\[/img:.*?\]",post_text).start()

                if re.search("\[attachment.*?-->(.*?)<!--.*?-->\[/attachment:.*?]",post_text):
                    attach_img=re.search("\[attachment.*?-->(.*?)<!--.*?-->\[/attachment:.*?]",post_text).start()
                    

                if (url_img)<(attach_img):
                    
                    first,second=re.compile("\[img:.*\].*?\[/img:.*?\]").split(post_text,1)
    
                    text='<para alignment="left"><font size=8>%s</font></para>' %(first)
                    report.append(Paragraph(text, styles["Normal"]))

                    img_url=re.findall("\[img:.*\](.*?)\[/img:.*?\]",post_text)[0]
                    img_name=re.findall(".*\/(.*\..*)",img_url)[0]

                    try:
                        img_name=str(len(img_to_delete))+img_name
                        urllib.urlretrieve(img_url, img_name)

                        img_to_delete.append(img_name)

                        im = Image(img_name)
                        if im.imageWidth > 500:
                            report.append(getImage(img_name,13*cm))
                        elif im.imageHeight > 500:
                            report.append(getImageH(img_name,13*cm))
                        else:
                            report.append(im)
                    except:
                        print("Could not retrieve Image (%s)"%(img_url))

                    post_text=second
                
                else:
                    first,second=re.compile("\[attachment.*?-->.*?<!--.*?-->\[/attachment:.*?]").split(post_text,1)

                    text='<para alignment="left"><font size=8>%s</font></para>' %(first)
                    report.append(Paragraph(text, styles["Normal"]))

                    image=get_picture(re.findall("\[attachment.*?-->(.*?)<!--.*?-->\[/attachment",post_text)[0])
                    image_list.append(image)
                    im = Image(image)
                    if im.imageWidth > 500:
                        report.append(getImage(image,13*cm))
                    elif im.imageHeight > 500:
                        report.append(getImageH(image,13*cm))
                    else:
                        report.append(im)

                    post_text=second

            text='<para alignment="left"><font size=8>%s</font></para>' %(post_text)
            report.append(Paragraph(text, styles["Normal"]))
        
        if post[1]==1:
            for pp in get_pictures_of_post(post[3]):
                if not str(picturePath+pp[0]) in image_list:
                    i = Image(picturePath+pp[0].decode('utf-8'))
                    if i.imageWidth > 500:
                        report.append(getImage(str(picturePath+pp[0]),13*cm))
                    elif i.imageHeight > 500:
                        report.append(getImageH(str(picturePath+pp[0]),13*cm))
                    else:
                        report.append(i)

                    report.append(Spacer(1, 5))

        report.append(Spacer(1, 8))
        report.append(HRFlowable(width="100%", thickness=1,color="GREY", spaceBefore=1, spaceAfter=1, hAlign='CENTER', vAlign='CENTER', dash=None))
        report.append(Spacer(1, 8))

    doc.build(report)

    for i_to_d in img_to_delete:
        try:
            os.remove(i_to_d)
        except OSError:
            print("Could not delete temporary Image")

buildDoc()

cursor.close()
cnx.close()
