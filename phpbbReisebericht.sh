#!/bin/bash

username="root"
password="password"
host="127.0.0.1"
datenbank="phpbb"
picturePath="/mnt/c/Users/neuru/Documents/5AHWII/INFI-DBP/phpbb-reportgenerator/phpbb/"

#Keine Datenbanktabelle zur auflösung bekannt
phpbbUserID=2

if [ $# -lt 2 ]
then
    echo 'Arguments topicID and phpbbUser missing'
else
    topicID=$1
    phpbbUser=$2
    filename=$3
    python reisebericht.py $username $password $host $datenbank $topicID $phpbbUser $phpbbUserID $picturePath $filename
fi